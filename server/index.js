const express = require('express')
const history = require('connect-history-api-fallback');
const app = express()
// app.use(history({ index: '/admin/index.html'}))



app.use(history({
    verbose: true,
    rewrites:[
        {from: /\/web\/.*$/,to: '/web/index.html'},
        {from: /\/admin\/.*$/, to: '/admin/index.html'}
    ],
    htmlAcceptHeaders: ['text/html', 'application/xhtml+xml','application/xml']
}))
app.set('secret', 'sdg15vz45a23a')
// 没有这项 得不到前端发送的post请求
app.use(express.json()) // for parsing application/json
app.use(require('cors')())
// 后台api
require('./routes/admin/index.js')(app)
require('./routes/web/index.js')(app)

// 托管上传的静态文件
app.use('/uploads', express.static(__dirname + '/uploads'))
app.use('/web', express.static(__dirname + '/web'))
app.use('/', express.static(__dirname + '/'))



app.listen(3000, ()=>{
    console.log('app is start');
})


