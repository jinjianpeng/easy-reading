const permissions = [
    {
        grade: "admin",
        permissions: {
            "/personalInfo": true,
            "/errorpage/401": false,
            "/errorpage/404": false,
            "/systemSet/permissionsManage": true,
            "/systemSet/loginLog": true,
            "/studentsManage/studentAdd": true,
            "/studentsManage/studentUpdate": true,
            "/studentsManage/studentList": true,
            "/novelsManage/novelList": true,
            "/novelsManage/novelAdd": true,
            "/novelsManage/novelUpdate": true,
            "/novelsManage/rollsManage/rollList": true,
            "/novelsManage/rollsManage/chaptersManage/chapterList": true,

            "/comicsManage/comicList": true,
            "/comicsManage/comicAdd": true,
            "/comicsManage/comicUpdate": true,
            "/comicsManage/chaptersManage/comicChapList": true,

            "/ebooksManage/ebookList": true,
            "/ebooksManage/ebookAdd": true,
            "/ebooksManage/ebookUpdate": true,
            "/usersManage/userList": true,
            "/usersManage/userAdd": true,
            "/usersManage/userUpdate": true,
            "/authorsManage/authorList": true,
            "/authorsManage/authorAdd": true,
            "/authorsManage/authorUpdate": true,

            "/reviewsManage/novelReview/novelWorkReview": true,
            "/reviewsManage/novelReview/novelContentReview": true,
            "/reviewsManage/comicReview/comicWorkReview": true,
            "/reviewsManage/comicReview/comicContentReview": true,
            "/reviewsManage/ebookReview/ebookWorkReview": true,
            "/reviewsManage/authorReview": true
        },
    },
    {
        grade:'author',
        permissions: {
            "/personalInfo": true,
            "/errorpage/401": false,
            "/errorpage/404": false,
            "/systemSet/permissionsManage": false,
            "/systemSet/loginLog": false,
            "/novelsManage/novelList": false,
            "/novelsManage/novelAdd": false,
            "/novelsManage/novelUpdate": false,
            "/novelsManage/rollsManage/rollList": false,
            "/novelsManage/rollsManage/chaptersManage/chapterList": false,

            "/comicsManage/comicList": true,
            "/comicsManage/comicAdd": true,
            "/comicsManage/comicUpdate": true,
            "/comicsManage/chaptersManage/comicChapList": true,
            
            "/ebooksManage/ebookList": true,
            "/ebooksManage/ebookAdd": true,
            "/ebooksManage/ebookUpdate": true,
            "/usersManage/userList": false,
            "/usersManage/userAdd": false,
            "/usersManage/userUpdate": false,
            "/authorsManage/authorList": false,
            "/authorsManage/authorAdd": false,
            "/authorsManage/authorUpdate": false,

            "/reviewsManage/novelReview/novelWorkReview": false,
            "/reviewsManage/novelReview/novelContentReview": false,
            "/reviewsManage/comicReview/comicWorkReview": false,
            "/reviewsManage/comicReview/comicContentReview": false,
            "/reviewsManage/ebookReview/ebookWorkReview": false
        },
    },
]

   
module.exports = permissions
