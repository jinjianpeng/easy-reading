const express = require('express')
const app = express()

app.use(require('cors')())
// 静态资源管理
app.use( '/', express.static('public'))
app.get('/person', function(req, res){
    res.send([
        {
            id: '4566',
            name:' 张三',
            age:'12',
            hobby:'play',

        }
    ])
})
app.listen(3001, ()=>{
    console.log('app is start');
})