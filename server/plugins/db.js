const mysql = require('mysql')
var pool = mysql.createPool({
    host     : 'localhost',
    user     : 'root',
    password : '123456',
    database : 'novel'
    });

    // 查询
let query = function(sql, values){
    return new Promise((resolve, reject)=>{
        pool.getConnection((err, connection)=>{
            if(err){
                reject(err)
            }else{
                connection.query(sql, values, (err, rows)=>{
                    if(err){
                        reject(err)
                    }else{
                        resolve(rows)
                        
                    }
                    connection.release()
                })
            }
        })
    })
}
module.exports = {query}