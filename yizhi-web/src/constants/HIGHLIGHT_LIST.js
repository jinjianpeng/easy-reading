export const highLightList = [
    {className:'hl-red',value:'#FFBA84'},
    {className:'hl-orange',value:'#E2943B'},
    {className:'hl-yellow',value:'#F7C242'},
    {className:'hl-green',value:'#86C166'},
    {className:'hl-blue',value:'#33A6B8'},
    {className:'hl-purple',value:'#8A6BBE'},
]