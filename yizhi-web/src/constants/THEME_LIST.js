export const backgroundList = [
    { name: "green", value: "rgb(197, 231, 207)" },
    { name: "beige", value: "rgb(233, 216, 188)" },
    { name: "white", value: "rgb(255, 255, 255)" },
    { name: "black", value: "rgb(0, 0, 0)" },
]

export const fontColors = [
    { name: "black", value: "rgb(0, 0, 0)" },
    { name: "white", value: "rgb(255, 255, 255)" },
    { name: "grey", value: "rgb(89, 68, 41)" },
    { name: "navy", value: "rgb(54, 80, 62)" },
]