export const fontFamilys = [
    { name: "默认字体", value: "SimSun" },
    { name: "微软雅黑", value: "Microsoft YaHei" },
    { name: "宋体", value: "SimHei" },
    { name: "Arial字体", value: "Arial" },
    { name: "Times New Roman", value: "Times New Roman" },
]

export const lineHeights = [
    { id: 0, value: '1' },
    { id: 1, value: '1.25' },
    { id: 2, value: '1.5' },
    { id: 3, value: '1.75' },
    { id: 4, value: '2' }
]

