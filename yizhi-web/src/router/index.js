import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

const Home = () => import('../views/Home/Home.vue');
const Novel = () => import('../views/Novel');
const Comics = () => import('../views/Comics');
const BBS = () => import('../views/BBS');
// homepage
const HomePage = () => import('../views/HomePage');
// homepage子路由
const MyShelf = ()=> import('../views/HomePage/HomePageComponent/HomePageContent/myshelf')
const MyFriend =()=> import('../views/HomePage/HomePageComponent/HomePageContent/myfriend')
const MyMessage = ()=>import('../views/HomePage/HomePageComponent/HomePageContent/mymessage')
const BaseInfo = ()=>import('../views/HomePage/HomePageComponent/HomePageContent/baseinfo')

const BookDetail = () => import('../views/BookDetail');
const comicDetail = () => import('../views/ComicDetail');
const Login = () => import("../views/Login");
const Register = () => import("../views/Register");
const EBook = () => import('../views/EBook');
const EBookContent = ()=>import('../views/EBook/EBookContent');
const EBookDetail = () => import('../views/EBookDetail');
const EBookShelf = () => import('../views/EBookShelf');
const EBookReader = () => import('../views/EBookReader');

// 申请作家
const ToAuthorOne = () => import('../views/Register/ToAuthorOne')
const ToAuthorTwo = () => import('../views/Register/ToAuthorTwo')

// Bookshelf 嵌套路由
const AllBooks = () => import('../views/EBookShelf/EBookShelfContent/BodyItem/AllBooks')
const FavoriteBooks = () => import('../views/EBookShelf/EBookShelfContent/BodyItem/FavoriteBooks')
const Highlights = () => import('../views/EBookShelf/EBookShelfContent/BodyItem/Highlights')
const Notes = () => import('../views/EBookShelf/EBookShelfContent/BodyItem/Notes')
const RecycleBin = () => import('../views/EBookShelf/EBookShelfContent/BodyItem/RecycleBin')

const Test = () => import('../views/TEST')
const liftTest = () => import('../views/TEST/lifeTest')

const NovelReader = () => import('../views/NovelReader');
const ComicReader = () => import('../views/ComicReader');
const Error404 = () => import('../views/Error404');
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    component: Home
  },
  {
    path: '/novel/:type',
    component: Novel
  },
  {
    path: '/comics',
    component: Comics
  },
  {
    path: '/bbs',
    component: BBS
  },
  {
    path: '/homepage',
    component: HomePage,
    children:[
      {path: 'myshelf', component:MyShelf},
      {path: 'mymessage', component:MyMessage},
      {path: 'myfriend', component:MyFriend},
      {path: 'baseinfo', component:BaseInfo},
    ]
  },
  {
    path: '/book/:id',
    component: BookDetail,
  },
  {
    path: '/book/:id/chapter/:chapid',
    component: NovelReader

  },
  {
    path: '/comic/:id',
    component: comicDetail
  },
  {
    path: '/comic/:id/chapter/:chapid',
    component: ComicReader
  },

  {
    path: '/signup/login',
    component: Login,
  },
  {
    path: '/signup/register',
    component: Register
  },
  {
    path:'/ebook',
    component: EBook,
    children:[
      { path: '/ebook/:type',
      component: EBookContent},
      {
        path: '/ebook/:type/:id',
        component: EBookDetail
      }
    ] 
  },
  
  {
    path: '/ebook-shelf',
    component: EBookShelf,
    children: [
      {
        path: 'all-books',
        component: AllBooks
      },
      {
        path: 'favorite-books',
        component: FavoriteBooks
      },
      {
        path: 'notes',
        component: Notes
      },
      {
        path: 'highlights',
        component: Highlights,
      },
      {
        path: 'recycle-bin',
        component: RecycleBin
      }
    ]
  },
  {
    path: '/ebook-reader/:bookid',
    component: EBookReader
  },
  {
    path: '/test',
    component: Test
  },
  {
    path: '/life-test',
    component: liftTest
  },
  {
    path: '/register/toauthorone',
    component: ToAuthorOne
  },
  {
    path: '/register/toauthortwo',
    component: ToAuthorTwo
  }
]


const router = new VueRouter({
  mode:'history',
  base: process.env.BASE_URL,
  routes
})

const whiteList = ['/signup/login', '/signup/register', '/404'];// 不重定向白名单
router.beforeEach((to, from, next) => {
  /* 必须调用 `next` */
  let token = localStorage.getItem('token') || ''
  let user_id = localStorage.getItem('id') || ''
  if (token) {
    if (to.path === '/login') {
      next({ path: '/' });
    } else if (to.path == '/404') {
      next();
    } else {
      if (!store.getters.userInfo) {
        store.dispatch('GetUserInfo',{user_id:user_id }).then(res => { // 拉取user_info
          console.log('已获取到用户信息', store.getters.userInfo)
          next();

        }).catch(err => {
          console.log(err);
        });
      } else {
        next()
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) { // 在免登录白名单，直接进入
      next()
    } else {
        next('/signup/login'); // 否则全部重定向到登录页
        Vue.prototype.$message(
          {
            type:'warning',
            message:'请先登录'
          }
        )
    }
  }
})

export default router
