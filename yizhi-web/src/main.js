import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import storage from '@/utils/storage'
import '@/utils/highlight'
import http from './http'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import VueLazyload from 'vue-lazyload'

Vue.config.productionTip = false
Vue.prototype.$storage = storage

Vue.use(ElementUI)
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error:require('@/assets/images/error.png'),
  loading: require('@/assets/images/img-loading.gif'),
  // the default is ['scroll', 'wheel', 'mousewheel', 'resize', 'animationend', 'transitionend']
  listenEvents: [ 'scroll' ]
})

Vue.prototype.$http = http
Vue.prototype.ENV = process.env.NODE_ENV === 'development'? '/' : '/web/'
console.log(Vue.prototype.ENV);

Vue.filter('simpleTime',(oldDate)=>{
  let date = new Date(oldDate);
  let year = date.getFullYear(),
      month = date.getMonth()+1,
      day = date.getDate(),
      h = date.getHours(),
      m = date.getMinutes()<10?'0'+date.getMinutes():date.getMinutes(),
      s = date.getSeconds()<10?'0'+date.getSeconds():date.getSeconds();
  return `${year}-${month}-${day} ${h}:${m}:${s}`        
});

Vue.mixin({
  methods:{
    undo(){
      Vue.prototype.$message({
        type:'warning',
        message:"功能暂未开发"
      })
    }
  }
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
