/* eslint-disable */
import storage from './storage'

var Pen = {
    highlighter: null
}

var classes = [
    'hl-red', 'hl-orange', 'hl-yellow', 'hl-green', 'hl-blue', 'hl-purple',
    'line-red', 'line-orange', 'line-yellow', 'line-green', 'line-blue', 'line-purple'
]
var classAppliers = []

function Highlight(chapterPos, highlight, characterRange) {
    this.chapterPos = chapterPos
    this.highlight = highlight
    this.characterRange = characterRange
}

Pen.init = function () {
    rangy.init(); // 初始化rangy模块
    classes.forEach(function (item) {
        var classApplier = rangy.createClassApplier(item, {
            ignoreWhiteSpace: true,
            elementTagName: 'span'
        })
        classAppliers.push(classApplier)
    })
}

Pen.create = function (document) {
    Pen.highlighter = rangy.createHighlighter(document); // 创建一个highlighter
    classAppliers.forEach(function (item) {
        Pen.highlighter.addClassApplier(item)
    })
}

/**
 * 
 * @param {*} that vue组件实例
 * @param {*} bookKey 书籍key
 * @param {*} selectedText 选中文字
 * @param {*} cfi 内容cfi定位
 * @param {*} color 选中高亮颜色
 * 
 * 注意： Pen.highlighter 每次选中高亮时插入数组中是按照 characterRange的范围前后插入的，而不是直接push或unshift
 */
// 增加 删除高亮后能找出本地数据库中对应的笔记
Pen.save = function (that, bookKey, selectedText, cfi, color, curCharacterRange, title, name) {
    console.log('selectedText', selectedText)
    var store = storage.get('highlight', {})

    store[bookKey] = store[bookKey] || []
    var chapterPos = that.section.index;
    console.log('保存')
    console.log(chapterPos)
    // 序列化是指 把当前Pen绑定的高亮样式数组都一起序列化
    var serStr = Pen.highlighter.serialize()
    console.log(serStr)

    //3月16 ++ 得到选择的字符集范围 (用户去除高亮时删除本地对应高亮)
    var characterRange = curCharacterRange
    
    var hlObj = new Highlight(chapterPos, serStr, characterRange)
    hlObj.id = '' + new Date().getTime()
    hlObj.createTime = new Date().getTime()
    hlObj.cfi = cfi
    hlObj.color = color
    hlObj.note = ''
    hlObj.selectedText = selectedText
    hlObj.title =  title
    hlObj.name = name

    console.log(hlObj)
    // 新加入的高亮制定 包括之前所有序列号高亮
    store[bookKey].unshift(hlObj)
    
    storage.set('highlight', store)
}

Pen.load = function (that, bookKey) {
    console.log('bookKey', bookKey)
    var store = storage.get('highlight', {})
    var hlObjs = store[bookKey]
    if (!hlObjs) return
    var chapterPos = that.section.index;
    // var result = []
    hlObjs.forEach(function (item) {
        if (item.chapterPos === chapterPos) {
            Pen.highlighter.deserialize(item.highlight)
        }
    })
    console.log(Pen.highlighter.highlights);
    // if (!result.length) return
   
}

Pen.clear = function (bookKey) {
    var store = storage.get('highlight', {})
    var hlObjs = store[bookKey]
    if (!hlObjs) return
    delete store[bookKey]
    storage.set('highlight', store)
}

// 去重
function unique(arr){
    let result = [],
        resultIndex = [];
    result = arr.filter(function(item, index ,arr){
        if(!resultIndex.includes(item.id)){
            resultIndex.push(item.id);
            return true;
        }else{
            return false;
        }
    })
    return result;
}
export default Pen;
