const BookMark = {};
let marks = null;

function Mark(cfi, title, content, progress){
    this.id = new Date().getTime();
    this.cfi = cfi 
    this.title = title
    this.content = content
    this.progress = progress
}

let addBookMark = BookMark.addBookMark = function(that, cfi, title, content, progress){
    marks = that.$storage.get('book-marks') || {};// 如果是第一次则执行右边为{}
    var key = that.$storage.get('recentBooks')[0];
    
    // 太耦合了 去掉 必须满足vue组件实例有locations才行
    // var cfi = that.locations.cfiFromLocation(that.locations.currentLocation);



    var mark = new Mark(cfi, title, content, progress)
    if(marks[key]){
        marks[key].push(mark);
    }else{
        marks[key] = [];
        marks[key].push(mark);
    }

    that.$storage.set("book-marks", marks)
}

let removeBookMark = BookMark.removeBookMark = function(that, markname){
    marks = that.$storage.get('book-marks') || {};
    var key = that.$storage.get('recentBooks')[0],
        markList = null;

    if(marks[key]){
        markList = marks[key].filter(element=>{
            return element.cfi !== markname;
        })

        marks[key] = markList;
        that.$storage.set('book-marks', marks);
    }
    
}

let removeBookMarks = BookMark.removeBookMarks = function(that){
    var key = that.$storage.get('recentBooks')[0];
    marks = that.$storage.get('book-marks') || {};
    
    if(marks[key]){
        delete marks[key];
        that.$storage.set('book-marks', marks);
    }

}

// let modifyBookMark = BookMark.modifyBookMark = function(that,){
//     var key = that.$storage.get('recentBook');

// }

let getBookMarks = BookMark.getBookMarks = function(that){
    var key = that.$storage.get('recentBooks')[0];
    marks = that.$storage.get('book-marks') || {};

    return marks[key];
}

export default BookMark;