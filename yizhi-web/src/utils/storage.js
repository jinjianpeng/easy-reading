const storage = {
    set (key, value) {
        // console.log('set', key, value)
        if (value === undefined || value === null) {
            localStorage.setItem(key, null)
            return
        }
        // 存入基本类型
        if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
            localStorage.setItem(key, JSON.stringify({
                _type: typeof value,
                value: value
            }))
        }
        // 存入对象
         else {
            localStorage.setItem(key, JSON.stringify(value))
        }
    },
    get (key, defaultValue) {
        // console.log('storage get ' + key)
        let item = localStorage.getItem(key)
        if (item === null) {
            return defaultValue || null
        }
        let ret = JSON.parse(item)
        // 得到基本类型
        if (ret && typeof ret === 'object') {
            if (ret._type === 'string' || ret._type === 'number' || ret._type === 'boolean') {
                return ret.value
            }
        }
        // 上面都不执行，则直接返回对象
        return ret
    },
    setItem (key, value) {
        this.set(key, value)
        return this
    },
    getItem (key) {
        return this.get(key)
    }
}

// let storage2 = new Proxy(storage, {
//   get: function (target, key, receiver) {
//     storage.get(key)
//     return Reflect.get(target, key, receiver)
//   },
//   set: function (target, key, value, receiver) {
//     storage.set(key, value)
//     return Reflect.set(target, key, value, receiver)
//   }
// })

export default storage
