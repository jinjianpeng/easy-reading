// arr1 为顺序
// arr2 排序
// recentBooks 和 books  bookkey 
function ArraySort(arr1, arr2, identifyProp){
    if(arr1.length == 0 || arr2.length == 0){
        return arr2;
    }
    let result = []
    arr1.forEach(element => {
        arr2 = arr2.filter(item=>{
            var flag = false;
            if(!flag && item[identifyProp] === element){
                flag = true;
                result.push(item);
                return false
            }else{
                return true;
            }
        })
    });
    return result;
}

export {
    ArraySort
}