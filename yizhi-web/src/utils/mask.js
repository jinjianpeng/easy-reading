function maskBox(parent){
    this.parentDom = document.querySelector(parent);
}
maskBox.prototype.init = function(){
    var body = document.body;
    var bodyW = body.offsetWidth;
    var bodyH = body.offsetHeight;

    var maskBox = document.createElement('div');
    maskBox.style.position = 'absolute';
    maskBox.style.left = "0px";
    maskBox.style.top = "0px";
    maskBox.className = 'maskbox';
    maskBox.style.width = '100px';
    maskBox.style.height = '100px';
    maskBox.style.background = '#000';
    this.parentDom.appendChild(maskBox);

}
export {
    maskBox
}