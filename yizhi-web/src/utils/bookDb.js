/* eslint-disable */
/**
 * key 数据库key
 * id  内容id
 */
class BookDb {

    constructor() {
        this.DB_NAME = 'EBook'
        this.OBJECT = 'EBook'
        this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    }

    init() {
        
        return new Promise((resolve, reject) => {
            let request = this.indexedDB.open(this.DB_NAME, '1')
            request.onerror = e => {
                console.log('连接数据库失败')
            }
            request.onupgradeneeded = e => {
                this.db = e.target.result
                //{ keyPath: 'id', autoIncrement: false } 不提供key，每次使用时自己添加
                this.db.createObjectStore(this.OBJECT)
                console.log('onupgradeneeded成功')
                resolve()
            }
            request.onsuccess = e => {
                console.log('onsuccess')
                this.db = e.target.result;
                console.log('创建对象仓库成功')
                resolve()
            }
        })
    }
    
    // 获取内容
    getItem(key, id){
        let t = this.db.transaction(this.OBJECT, 'readonly')
        let store = t.objectStore(this.OBJECT)
        var req = store.get(key);

        return new Promise((resolve, reject)=>{
            req.onsuccess = e => {
                console.log(e.target)
                let result = e.target.result,
                    res = '';
                
                // 找到相应内容
                result.forEach(element=>{
                    if(element['key']===id){
                        res = element;
                        console.log("找到了----------------------");
                        resolve(res);
                    }
                })
                
            };
        })
        
    }

    // 获取所有内容
    getItems(key){
        let t = this.db.transaction(this.OBJECT, 'readonly')
        let store = t.objectStore(this.OBJECT)
        var req = store.get(key);

        return new Promise((resolve, reject)=>{
            req.onsuccess = e => {
                resolve(e.target.result)
                
            };
        })
        
    }

    // 第一次插入
    _first_addItem(key, item){
        
        let t = this.db.transaction(this.OBJECT, 'readwrite'),
        store = t.objectStore(this.OBJECT),
        req = store.put([item], key)
    
        return new Promise((resolve,rejct)=>{
            req.onsuccess = () => {
                console.log('数据添加成功~~~')
                resolve(true)
            }
        })
        
        
        
    }

    // 添加内容
    async addItem(key, item, verification){
        // 获取所有结果
        let result = await this.getItems(key);
        console.log('result ----------', typeof result);
        // 第一次插入
        if(!result || typeof result == undefined){
            return await this._first_addItem(key, item);
        }
        // 有数据插入
        let flag = false,
            editIndex = '';
        for(let i=0; i<result.length; i++){
            if(result[i][verification] === item[verification]){
                flag = true;
                editIndex = i;
            }
        }
        // 不存在
        if(flag === false){
            result.push(item);
            let t = this.db.transaction(this.OBJECT, 'readwrite'),
                store = t.objectStore(this.OBJECT),
                req = store.put(result, key)
            
            req.onsuccess = () => {
                console.log('数据添加成功~~~')
            }
            req.onerror = ()=>{
                console.log('数据添加失败~~~')
            }
            return true;

        }
        // 存在
        else{
            // 是修改吗？
            if(arguments[3]){
                result[editIndex] = item;
                let t = this.db.transaction(this.OBJECT, 'readwrite'),
                store = t.objectStore(this.OBJECT),
                req = store.put(result, key)
            
                req.onsuccess = () => {
                    console.log('数据修改成功~~~')
                    return true;
                }
            }

            console.log('数据已经存在~~~')
             // 用于组织内容写入数据库
            return false;
        }
    }

    // 修改内容
    // async updateItem(key, item, verification){
    //     return await this.addItem(key, item, verification, true)
    // }

    // 覆盖更新
    updateItems(key, items){
        let t = this.db.transaction(this.OBJECT, 'readwrite'),
            store = t.objectStore(this.OBJECT),
            req = store.put(items, key);
            
        return new Promise((resolve, reject)=>{
            req.onsuccess = (e)=>{
                console.log(e);
                resolve()
            }
        })
    }

    // 删除内容
    deleteItem(key, id){
        let t = this.db.transaction(this.OBJECT, 'readwrite')
        let store = t.objectStore(this.OBJECT)
        var req = store.get(key);

        return new Promise((resolve, reject)=>{
            req.onsuccess = e => {
                console.log(typeof e.target.result)
                // 反序列化
                let result = e.target.result,
                    res = '';

                result.forEach((element,index)=>{
                    if(element['key']===id){
                        result.splice(index, 1);
                        console.log("删除成功~~~");
                        store.put(result, key)
                        resolve(true);
                    }
                })
            };
        })
    }

    // 获得数据
    getContent(key){
        return new Promise((resolve, reject)=>{
            let t = this.db.transaction(this.OBJECT, 'readonly')
            let store = t.objectStore(this.OBJECT)
            var req = store.get(key);

            req.onsuccess = e => {
                console.log("获得书籍数据成功~~~");
                resolve(e.target.result)
            }
        }) 
    }
    // 删除数据
    deleteContent(key){
        return  new Promise((resolve, reject)=>{
            let t = this.db.transaction(this.OBJECT, 'readwrite')
            let store = t.objectStore(this.OBJECT)
            var req = store.delete(key);

            req.onsuccess = e => {
                console.log("删除书籍数据成功~~~");
                resolve()
            }
        }) 
    }
    // 添加数据
    addContent(key, content){
        return new Promise((resolve, reject)=>{
            let t = this.db.transaction(this.OBJECT, 'readwrite')
            let store = t.objectStore(this.OBJECT)
            var req = store.put(content, key);
        
            req.onsuccess = e => {
                console.log("添加书籍数据成功~~~");
                resolve()
            }
        })    
    }

    // // 清空所有书籍
    // clear(key, items){
    //     return new Promise((resolve, reject)=>{
    //         let t = this.db.transaction(this.OBJECT, 'readwrite')
    //         let store = t.objectStore(this.OBJECT)
            
    //     })
    // }


}
let db = new BookDb()

export default db
