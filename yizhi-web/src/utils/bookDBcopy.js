/* eslint-disable */

class BookDb {

    constructor() {
        this.DB_NAME = 'EBook'
        this.OBJECT = 'EBook'
        this.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    }

    init() {
        
        return new Promise((resolve, reject) => {
            let request = this.indexedDB.open(this.DB_NAME, '1')
            request.onerror = e => {
                console.log('连接数据库失败')
            }
            request.onupgradeneeded = e => {
                this.db = e.target.result
                //{ keyPath: 'id', autoIncrement: false } 不提供key，每次使用时自己添加
                this.db.createObjectStore(this.OBJECT)
                console.log('onupgradeneeded成功')
                resolve()
            }
            request.onsuccess = e => {
                console.log('onsuccess')
                this.db = e.target.result;
                console.log('创建对象仓库成功')
                resolve()
            }
        })
    }


    getBooks(id) {
        return new Promise((resolve, reject) => {
            let t = this.db.transaction(this.OBJECT, 'readwrite')
            let store = t.objectStore(this.OBJECT)
            var req = store.get(id);

            req.onsuccess = e => {
                console.log(e.target)
                resolve( e.target.result);
            };
        })
    }


    getBook(id, keyProp) {
        return new Promise((resolve, reject) => {
            let tx = this.db.transaction(this.OBJECT, 'readwrite')
            let store = tx.objectStore(this.OBJECT)
            let req = store.get(keyProp);
            req.onsuccess = e => {
                let result = JSON.parse(e.target.result)
                result.forEach(element => {
                    if(element.key == id){
                        resolve(element)
                    }
                });
                
            }
        })
    }

    addBook(book, keyProp) {
        return new Promise((resolve, reject) => {
            let t = this.db.transaction(this.OBJECT, 'readwrite')
            let store = t.objectStore(this.OBJECT)
            let req = store.put(book, keyProp)
            req.onsuccess = () => {
                console.log('保存成功')
                resolve()
            }
        })
    }

    deleteBook(keyProp) {
        return new Promise((resolve, reject) => {
            let t = this.db.transaction(this.OBJECT, 'readwrite')
            let store = t.objectStore(this.OBJECT)
            let req = store.delete(keyProp)
            req.onsuccess = () => {
                console.log('删除数据成功')
                resolve()
            }
        })
    }

    updateBook(book, keyProp) {
        return this.addBook(book, keyProp);
    }
}

let db = new BookDb()

export default db
