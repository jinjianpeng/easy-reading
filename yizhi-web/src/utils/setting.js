import {backgroundList, fontColors} from '@/constants/THEME_LIST';
import {fontFamilys, lineHeights} from '@/constants/FONT_LIST';

let Setting = {};

// 传入arr数组  key 键属性名 value 键属性值  
Setting.getThemeValue = function(arr, key, value){
    for(let i=0; i<arr.length; i++){
        if(arr[i][key] == value){
            return arr[i]['value'];
        }
    }
}

// 初始化默认样式
Setting.initStyle = function(that){
    let options = that.$storage.get('setting');

    if(!options){
        options = {
            fontSize:14,
            fontColor:'rgb(0, 0, 0)',
            bgColor:'green',
            fontFamily:'默认字体',
            lineHeight:1
        }
    }
    console.log(options);
    that.$store.commit('SET_FONTSIZE',options.fontSize);
    that.$store.commit('SET_FONTCOLOR',options.fontColor);
    that.$store.commit('SET_BGCOLOR',options.bgColor);
    that.$store.commit('SET_FONTFAMILY',options.fontFamily);
    that.$store.commit('SET_LINEHEIGHT',options.lineHeight);

    Setting.book =  that.$store.state.reader.book;
    Setting.themes = that.$store.state.reader.themes;
    this.rendition(that);

}

// 写入到localstorage
function _insertLocal(that, category, key){
    let res = that.$storage.get('setting') || {};
    res[category] = key;
    // console.log("res----------",res);
    that.$storage.set("setting", res);
}

// 设置字体大小
Setting.setFontSize = function(that, key){
    that.$store.commit('SET_FONTSIZE',key)
    renderFontSize(key)
    console.log(key);
    _insertLocal(that, "fontSize", key)

}
// 设置字体颜色
Setting.setFontColor = function(that, value){
    that.$store.commit('SET_FONTCOLOR',value);

    renderFontColor(value)

    _insertLocal(that, "fontColor", value);
}

// 设置背景颜色
Setting.setBgColor = function(that, key){
    that.$store.commit('SET_BGCOLOR',key);

    renderBgColor(key)

    _insertLocal(that, "bgColor", key);
}
//设置字体家族
Setting.setFontFamily = function(that, key){
    that.$store.commit('SET_FONTFAMILY',key);

    renderFontFamily(key)

     _insertLocal(that, "fontFamily", key);
}
// 设置行高
Setting.setLineHeight = function(that, val){
    val = parseFloat(val);
    that.$store.commit('SET_LINEHEIGHT',val);

    renderLineHeight(val);

    _insertLocal(that, "lineHeight", val);
}

// 渲染页面
Setting.rendition = function(that){
    console.log("SEtting.rendition 执行");
    let setting = that.$store.state.reader.setting;

    renderFontSize(setting.fontSize)
    renderFontFamily(setting.fontFamily)
    renderFontColor(setting.fontColor)
    renderBgColor(setting.bgColor)
    
    try{
        renderLineHeight(setting.lineHeight)
    }catch(e){

    }
    that.$storage.set('setting',setting)
}

function renderBgColor(bgColor){
    let bgDom = document.querySelector('.background');
    bgDom.style.background =  Setting.getThemeValue(backgroundList, 'name', bgColor)
}

function renderFontFamily(fontFamily){
    let val =  Setting.getThemeValue(fontFamilys, 'name', fontFamily);
    Setting.themes.font(val);
}

function renderFontSize(fontSize){
    Setting.themes.fontSize(fontSize + "px");
}

function renderFontColor(color){
    Setting.themes.override("color", color, true);
}

function renderLineHeight(lineHeight){
    initLineHeight(lineHeight);
}
function initLineHeight(lineHeight) {
    let style,
      iframe = document.querySelector("iframe");

    if (iframe.contentDocument.getElementById("style-setting")) {
      style = iframe.contentDocument.getElementById("style-setting");
    } else {
      style = iframe.contentDocument.createElement("style");
      style.id = "style-setting";
      iframe.contentDocument.head.appendChild(style);
    }

    var val = "p {line-height:" +lineHeight + "em !important;}";
    style.innerHTML = val;
    iframe.contentDocument.head.appendChild(style);
  }

export default Setting;