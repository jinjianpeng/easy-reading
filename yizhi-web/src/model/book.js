class Book{
    constructor(key, name, author, description, cover, publisher, md5){
        this.key = key;
        this.name = name;
        this.author = author;
        this.description = description;
        this.cover = cover;
        this.publisher = publisher;
        this.md5 = md5;
    }
}
export default Book;