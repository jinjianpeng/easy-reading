class BookMark{
    constructor(bookKey, cfi, name, chapter){
        this.key = new Date().getTime() + ""; // 唯一的键
        this.bookKey = bookKey; // 所属的书籍的键
        this.cfi = cfi; // 标记阅读位置的cfi
        this.name = name; // 此项书签的别名
        this.chapter = chapter; // 此项书签的摘要
    }
}
export default BookMark