import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import mutations from './mutations'
// modules子模块导入
import ebook from './module/ebook';
import reader from './module/reader';
import user from './module/user';
import home from './module/home';
import novel from './module/novel';
import comic from './module/comic'
Vue.use(Vuex)


export default new Vuex.Store({
  state: state,
  mutations: mutations,
  actions: {
  },
  modules: {
    ebook:ebook,
    reader:reader,
    user:user,
    home:home,
    novel,
    comic
  }
})
