export default {
    state: {
        chapters: [],
        book: '',
        setting: {
            bgColor: '',
            fontColor: '',
            fontSize: '',
            fontFamily: '',
            lineHeight: '',
        },
        bookKey:'', // 渲染页面的书籍key
        meta:'',    // 书籍封面数据
        rendition:'',// 渲染模块
        progress:'', //进度条
        locations:'', // 位置
        themes:'', // 主题
        section:'',//当前页 或部分

        bookmarks:[], //书签

        curcfi:'', //当前页面的cfi

        arraybuff:'',
        loading: true,

    },

    mutations: {
        // 去掉保存阅读位置记录，增加全局加载中状态
        SET_LOADING(state, loading){
            state.loading = loading;
        },

        // 设置当前书籍文件缓存区
        SET_ARRAYBUFF(state, arraybuff){
            state.arraybuff = arraybuff
        },

        // 设置书签
        SET_BOOKMARKS(state, bookmarks){
            state.bookmarks = (bookmarks)
        },

        // 设置当前页面的CFI
        SET_CURCFI(state, cfi){
            state.curcfi = cfi;
        },

        // 设置EPUB 类实例
        SET_SECTION(state, section){
            state.section = section;
        },
        SET_THEMES(state, themes){
            state.themes = themes;
        },
        SET_LOCATIONS(state, locations){
            state.locations = locations;
        },
        SET_PROGRESS(state, progress){
            state.progress = progress;
        },
        SET_RENDITION(state, rendition){
            state.rendition = rendition;
        },
        SET_META(state, meta){
            state.meta = meta;
        },
        SET_CHAPTERS(state, chapters) {
            state.chapters = chapters;
        },
        SET_BOOK(state, book) {
            state.book = book;
        },
        SET_BOOKKEY(state, bookkey){
            state.bookKey = bookkey;
        },

        // 设置 setting
        SET_BGCOLOR(state, bgcolorKey){
            console.log("我是state",state);
            state.setting.bgColor = bgcolorKey;
        },
        SET_FONTSIZE(state, fontsizeKey){
            state.setting.fontSize = fontsizeKey;
        },
        SET_FONTCOLOR(state, fontcolorKey){
            state.setting.fontColor = fontcolorKey;
        },
        SET_FONTFAMILY(state, fontfamilyKey){
            state.setting.fontFamily = fontfamilyKey;
        },
        SET_LINEHEIGHT(state, lineheightKey){
            state.setting.lineHeight = lineheightKey;
        },

    },
    getters:{
        Loading: state=> state.loading,
        Meta: state=>state.meta
    }
}