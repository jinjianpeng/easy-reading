import state from "../state";

export default {
    state:{
        ebooks:[], //书架上ebooks
        showWay:'ListWay',
        ebooklist:[],  //书城ebooks
        ebookDetail:{}, //ebook细节页
        navIndex:0,//控制banner显示的索引
        total:0, //电子书总页数
        tabbars: [
            { name: "全部图书", iconFont:"\ue9db", index: 0 , url:'/ebook-shelf/all-books'},
            { name: "我的喜爱", iconFont: "\ue62f", index: 1, url:'/ebook-shelf/favorite-books'},
            { name: "我的笔记", iconFont: "\ueb69", index: 2, url:'/ebook-shelf/notes'},
            { name: "我的书摘", iconFont: "\ue7cd", index: 3, url:'/ebook-shelf/highlights'},
            { name: "我的回收", iconFont: "\ue7f2", index: 4, url:'/ebook-shelf/recycle-bin'},
          ],
        recentEbooks:[],
        favoriteEbooks:[],
        recycleEbooks:[]
    },

    mutations:{
        // 设置我的所有图书
        SET_RECENTEBOOKS(state, recentEbooks){
            state.recentEbooks = recentEbooks
        },
        // 设置我的笔记
        SET_FAVORITEEBOOKS(state, favoriteEbooks){
            state.favoriteEbooks = favoriteEbooks
        },
        // 设置我的回收
        SET_RECYCLEEBOOKS(state, recycleEbooks){
            state.recycleEbooks = recycleEbooks
        },

        // 设置电子书展示列表
        SET_EBOOKS(state, ebooksArg){
            state.ebooks = ebooksArg;
        },

        // 设置电子书展示方式（卡片、列表、封面）
        SET_SHOWWAY(state, showWay){
            state.showWay = showWay
        },

        SET_EBOOKLIST(state, ebooklist){
            state.ebooklist = ebooklist
        },

        SET_EBOOKDETAIL(state, ebookDetail){
            state.ebookDetail = ebookDetail
        },

        SET_NAV_INDEX(state, navIndex){
            state.navIndex = navIndex
        },

        SET_TOTAL(state, total){
            state.total = total
        }
    },

    getters:{
        Ebooks: state=> state.ebooks,
        EbookList: state=> state.ebooklist,
        EbookDetail: state=> state.ebookDetail,
        NavIndex: state=> state.navIndex,
        Total: state=> state.total,
        Tabbars: state =>state.tabbars,

        RecentEbooks: state=> state.recentEbooks,
        FavoriteEbooks: state=> state.favoriteEbooks,
        RecycleEbooks: state=> state.recycleEbooks,
    }
} 