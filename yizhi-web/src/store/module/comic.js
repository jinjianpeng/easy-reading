export default {
    state:{
        // 漫画细节
        comic_detail:{},
    },
    mutations:{
        SET_COMIC_DETAIL(state, comic_detail){
            state.comic_detail = comic_detail
        }
    },
    getters:{
        comicDetail: state => state.comic_detail
    }
}