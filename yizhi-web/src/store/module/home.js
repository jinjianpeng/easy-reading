export default {
    state:{
        zhaiwen:[]
    },
    mutations:{
        SET_ZHAIWEN(state, zhaiwen){
            state.zhaiwen = zhaiwen
        }
    },
    getters: {
        getZhaiWen: state => {
          return state.zhaiwen
        }
    },
}