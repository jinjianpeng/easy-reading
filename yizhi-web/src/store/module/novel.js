export default {
    state:{
        // 分类小说
        type_novels:null,
        // 小说细节
        novel_detail:{},
        // 小说设置
        novel_setting:{
            theme:'',
            fontFamily:'',
            defaultSize:''
        },
        dayIndex:0, //更新天数索引
        otherIndex:0, // 其他选择索引
        classifyIndex:0, // 分类索引
        themes: [
            { name: "default", bgc: "#faf0d9" },
            { name: "green", bgc: "#dbeeda" },
            { name: "blue", bgc: "#e9fdfe" },
            { name: "white", bgc: "#fff" },
            { name: "gray", bgc: "#dfd6d7" },
            { name: "pink", bgc: "#fdd9d9" },
            { name: "night", bgc: "#323536" },
          ],
          fontFamily:[
            {name:'serif',value:"sans-serif"},
            {name:'草书',value:"cursive"},
            {name:'艺术字体',value:"fantasy"},
            {name:'仿宋',value:"fangsong"},
            {name:'微软雅黑',value: "Microsoft Yahei"}
        ],
        defaultSize:14,
    },
    mutations:{
        SET_DAYINDEX(state, dayIndex){
            state.dayIndex = dayIndex
        },
        SET_OTHERINDEX(state, otherIndex){
            state.otherIndex = otherIndex
        },
        SET_CLASSIFYINDEX(state, classifyIndex){
            state.classifyIndex = classifyIndex
        },

        SET_TYPE_NOVELS(state, type_novels){
            state.type_novels = type_novels
        },
        SET_NOVEL_DETAIL(state, novel_detail){
            state.novel_detail = novel_detail
        },
        SET_NOVEL_SETTING(state, novel_setting){
            state.novel_setting = novel_setting
        },
        SET_NOVEL_SETTING_SIZE(state, defaultSize){
            state.novel_setting.defaultSize = defaultSize
        },
        SET_NOVEL_SETTING_FONT(state, fontFamily){
            state.novel_setting.fontFamily = fontFamily
        },
        SET_NOVEL_SETTING_THEME(state, theme){
            state.novel_setting.theme = theme
        }
    },
    getters:{
        typeNovels: state => state.type_novels,
        novelDetail: state => state.novel_detail,
        novelSetting: state => state.novel_setting,

        // 得到设置选项
        Themes: state=> state.themes,
        FontFamily: state=> state.fontFamily,
        DefaultSize: state=> state.defaultSize,

        // 书库分类 index选项
        DayIndex: state=> state.dayIndex,
        OtherIndex: state=> state.otherIndex,
        ClassifyIndex: state=> state.classifyIndex
    }
}