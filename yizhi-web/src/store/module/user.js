import axios from 'axios'
export default {
    state:{
        user:null,
        archivesMenuIdx:0
    },
    mutations:{
        SET_USER(state, user){
            state.user = user
        },
        SET_ARCHIVES_MENU_IDX(state, idx){
            state.archivesMenuIdx = idx
        }
    },
    getters: {
        userInfo: state => {
          return state.user
        },
        ArchivesMenuIdx: state=> {
            return state.archivesMenuIdx
        }
    },
    actions:{
        async GetUserInfo({commit, state}, obj){
            const {user_id} = obj
            let url = 'http://localhost:3000/web/api/user'
            let res = await axios.get(url + '?user_id=' + user_id)
            commit('SET_USER',res.data)

        }
    }
}