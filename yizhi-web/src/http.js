import axios from 'axios'
import Vue from 'vue'
import router from './router'

const http = axios.create({
  baseURL: process.env.VUE_APP_API_URL || '/web/api'
    // baseURL: 'http://localhost:3000/web/api/'
})

http.interceptors.request.use(function(config){
    console.log(config);
    if(localStorage.token){
        config.headers.Authorization = 'Bearer' + localStorage.token
    }
    return config
},function(error){
    return Promise.reject(error)
})
http.interceptors.response.use(res => {
  console.log(res);
    return res
  }, err => {
    console.log(err.response.data.message);
    if (err.response.data.message) {
      Vue.prototype.$message({
        type: 'error',
        message: err.response.data.message
      })
      
      if (err.response.status === 401) {
        router.push('/signup/login')
      }
    }
    
    return Promise.reject(err)
  })

export default http;