export default {
    state:{
        ebook:null,
    },
    mutations:{
        SET_EBOOK(state, ebook){
            state.ebook = ebook
        }
    }
}