export default {
    state:{
        comic:{}
    },
    mutations:{
        SET_COMIC(state, comic){
            state.comic = comic
        }
    }
}