
const getters = {
  uid: state => state.user.uid,     //用户id
  token: state => state.user.token, //用户令牌
  userInfo: state =>state.user.userInfo,//用户所有信息
  User: state =>state.user.user, // 当前编辑的用户
  Author: state =>state.user.author, //当前编辑的作者
  Ebook: state=> state.ebook.ebook, // 当前编辑的电子书
  Novel:state=> state.novel.novel, // 当前编辑的小说
  Comic:state=> state.comic.comic, // 当前编辑的漫画

  sidebar: state => state.app.sidebar,
  visitedViews: state => state.app.visitedViews, //快速导航tab
  
  permission_routers: state => {
    // 对应permission.js中的state.routers
        
            return  state.permission.routers;
       
   }, 
  addRouters: state => state.permission.addRouters
};
export default getters
