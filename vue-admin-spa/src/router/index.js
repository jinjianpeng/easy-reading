import Vue from 'vue';
import Router from 'vue-router';
console.log(process.env.NODE_ENV)
const _import = require('./_import_' + process.env.NODE_ENV);
// in development env not use Lazy Loading,because Lazy Loading large page will cause webpack hot update too slow
// 所以只在生产中使用延迟加载

/* layout */
import Layout from '../views/layout/Layout';

/* login */
const Login = _import('login/index');
const authRedirect = _import('login/authredirect');
const sendPWD = _import('login/sendpwd');
const reset = _import('login/reset');

/* components */
// const Tinymce = _import('components/tinymce');
// const Mixin = _import('components/mixin');

/* error page */
const Err404 = _import('error/404');
const Err401 = _import('error/401');


const PersonalInfo = _import('index/personalInfo');
// const PersonalInfo = ()=> import('../views/index/personalInfo')
const Readme = _import('index/readme');
/* example*/
// const TableList = _import('example/tableList');

// 豆瓣电影
// const NewMovie = _import('movie/newMovie');
// const MovieSearch = _import('movie/movieSearch/index');


// const Form = _import('example/form');
// const Tinymce = _import('example/tinymce');
// const Mixin = _import('example/mixin');

/* 系统管理*/
const PermissionsManage = _import('systemSet/permissionsManage');
/* 学生管理*/
// const StudentList = _import('student/studentInfo/index');
// const StudentAdd = _import('student/studentInfo/modal/studentAdd'); /*新增修改后期改成同一个弹窗*/
// const StudentUpdate = _import('student/studentInfo/modal/studentUpdate');

/* 机构管理*/
// const OrgManager = _import('org-manage/org-manager/index');




/** 小说管理 */
const NovelList = _import('novelsManage/novelList')
const NovelAdd = _import('novelsManage/novelAdd')
const NovelUpdate = _import('novelsManage/novelUpdate')

/**  卷管理*/
const RollList = _import('novelsManage/rollsManage/rollList')

/***章节管理 */
const ChapList = _import('novelsManage/rollsManage/chaptersManage/chapterList')


/** 漫画管理 */
const ComicList = _import('comicsManage/comicList')
const ComicAdd = _import('comicsManage/comicAdd')
const ComicUpdate = _import('comicsManage/comicUpdate')
/** 章节管理 */
const ComicChapList = _import('comicsManage/chaptersManage/comicChapList')

/** 电子书管理 */
const EbookList = _import('ebooksManage/ebookList')
const EbookAdd = _import('ebooksManage/ebookAdd')
const EbookUpdate = _import('ebooksManage/ebookUpdate')

/** 用户管理 */
const UserList = _import('usersManage/userList')
const UserAdd = _import('usersManage/userAdd')
const UserUpdate = _import('usersManage/userUpdate')

/** 作家管理 */
const AuthorList = _import('authorsManage/authorList')
const AuthorAdd = _import('authorsManage/authorAdd')
const AuthorUpdate = _import('authorsManage/authorUpdate')

/**审核管理 */
const NovelReivew = _import('reviewsManage/novelReview/index')
const NovelWorkReview = _import('reviewsManage/novelReview/novelWorkReview')
const NovelContentReview = _import('reviewsManage/novelReview/novelContentReview')
const ComicReivew = _import('reviewsManage/comicReview/index')
const ComicWorkReview = _import('reviewsManage/comicReview/comicWorkReview')
const ComicContentReview = _import('reviewsManage/comicReview/comicContentReview')
const EbookWorkReview = _import('reviewsManage/ebookReview/ebookWorkReview')
const AuthorReview = _import('reviewsManage/authorReview/index')

Vue.use(Router);

 /**
  * icon : 菜单图标
  * hidden : true不显示在菜单栏
  * redirect : noredirect 为不重定向
  * noDropdown : true 不显示子菜单
  * meta : { role: ['admin'] }  will control the page role
  **/

const constantRouterMap = [
  { path: '/login', component: Login, hidden: true },
  { path: '/authredirect', component: authRedirect, hidden: true },
  { path: '/sendpwd', component: sendPWD, hidden: true },
  { path: '/reset', component: reset, hidden: true },
  { path: '/404', component: Err404, hidden: true },  //假地址时重定向
  { path: '/401', component: Err401, hidden: true },  //无权限时重定向

  // {
  //   path: '/',
  //   //component: Layout,
  //   // redirect: '/index/readme',  //重定向到默认首页
  //   redirect: '/index/personalInfo',
   
  //   hidden: true,
    
  // },
 
  {
    path: '/',
    component: Layout,
    redirect: '/personalInfo',
    name: '',
    // icon: 'EXCEL',
    noDropdown: true,
    children: [
        // { path: 'readme', component: Readme, name: '系统说明' },
        { path: 'personalInfo', component: PersonalInfo, name: '个人信息' }
    ]
  },
  
  

  // {
  //   path: '/movie',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '',
  //   // icon: 'EXCEL',
  //   noDropdown: true,
  //   children: [
  //       { path: 'newMovie', component: NewMovie, name: '热映电影列表' },
  //       { path: 'movieSearch', component: MovieSearch, name: '电影搜索' }
  //   ]
  // },
 
  {
    path: '/errorpage',
    component: Layout,
    redirect: 'noredirect',
    name: '错误页面',
    // icon: '404',
    children: [
      { path: '401', component: Err401, name: '401' },
      { path: '404', component: Err404, name: '404' }
    ]
  },
  {
    path: '/systemSet',
    component: Layout,
    redirect: 'noredirect',
    name: '系统设置',
    // icon: '404',
    children: [
      { path: 'permissionsManage', component: PermissionsManage, name: '权限管理' },
      
    ]
  },  

  // {
  //   path: '/studentsManage',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '小说管理1',
  //   // icon: '404',
  //   children: [
  //     { path: 'studentList', component: StudentList, name: '小说列表' },
  //     { path: 'studentAdd', component: StudentAdd, name: '小说添加' },
  //     { path: 'studentUpdate', component: StudentUpdate, name: '小说修改' },
      
  //   ]
  // },
  {
    path: '/novelsManage',
    component: Layout,
    redirect: 'noredirect',
    name: '小说管理',
    // icon: '404',
    children: [
      { path: 'novelList', component: NovelList, name: '小说列表' },
      { path: 'novelAdd', component: NovelAdd, name: '小说添加' },
      { path: 'novelUpdate', component: NovelUpdate, name: '小说修改' },  
      {
        path:'rollsManage/rollList',
        component: RollList,
        name:'卷列表'
      },
      {
        path:'rollsManage/chaptersManage/chapterList',
        component: ChapList,
        name:'章节列表'
      },
    ]
  },
 
  
  {
    path: '/comicsManage',
    component: Layout,
    redirect: 'noredirect',
    name: '漫画管理',
    // icon: '404',
    children: [
      { path: 'comicList', component: ComicList, name: '漫画列表' },
      { path: 'comicAdd', component: ComicAdd, name: '漫画添加' },
      { path: 'comicUpdate', component: ComicUpdate, name: '漫画修改' },  
      { path: 'chaptersManage/comicChapList', component: ComicChapList, name: '漫画章节列表'}
    ]
  },

  {
    path: '/ebooksManage',
    component: Layout,
    redirect: 'noredirect',
    name: '电子书管理',
    // icon: '404',
    children: [
      { path: 'ebookList', component: EbookList, name: '电子书列表' },
      { path: 'ebookAdd', component: EbookAdd, name: '电子书添加' },
      { path: 'ebookUpdate', component: EbookUpdate, name: '电子书修改' },  
    ]
  },

  {
    path: '/usersManage',
    component: Layout,
    redirect: 'noredirect',
    name: '用户管理',
    // icon: '404',
    children: [
      { path: 'userList', component: UserList, name: '用户列表' },
      { path: 'userAdd', component: UserAdd, name: '用户添加' },
      { path: 'userUpdate', component: UserUpdate, name: '用户修改' },  
    ]
  },

  {
    path: '/authorsManage',
    component: Layout,
    redirect: 'noredirect',
    name: '作家管理',
    // icon: '404',
    children: [
      { path: 'authorList', component: AuthorList, name: '用户列表' },
      { path: 'authorAdd', component: AuthorAdd, name: '用户添加' },
      { path: 'authorUpdate', component: AuthorUpdate, name: '用户修改' },  
    ]
  },

  {
    path: '/reviewsManage',
    component: Layout,
    redirect: 'noredirect',
    name: '审核管理',
    // icon: '404',
    children: [
      { 
        path: 'novelReview', name: '小说审核' ,component: NovelReivew,
        children:[
          {path: 'novelWorkReview', component:NovelWorkReview, name:"小说作品审核"},
          {path: 'novelContentReview', component:NovelContentReview, name:"小说内容审核"}
        ] 
      },
      { 
        path: 'comicReview',  name: '漫画审核',component: ComicReivew,
        children:[
          {path: 'comicWorkReview', component:ComicWorkReview, name:"漫画作品审核"},
          {path: 'comicContentReview', component:ComicContentReview, name:"漫画内容审核"}
        ]   
      },
      { path: 'ebookReview/ebookWorkReview', component: EbookWorkReview, name: '电子书审核' }, 
      { path: 'authorReview',component: AuthorReview, name: '作者审核'} 
    ]
  },

  // {
  //   path: '/orgManage',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '机构管理',
  //   children: [
  //     { path: 'orgManager', component: OrgManager, name: '机构列表' },
     
  //   ]
  // },
  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: 'noredirect',
  //   name: '综合实例',
  //   // icon: 'zonghe',
  //   children: [
  //     { path: 'tableList', component: TableList, name: '示例表格' },
  //     { path: 'form', component: Form, name: 'form表单编辑' },

  //     { path: 'tinymce', component: Tinymce, name: '富文本编辑器' },
  //     { path: 'mixin', component: Mixin, name: '小组件' },
  //     { path: '31', component: Form, name: '三级菜单1' },
  //   ]
  // },
  // { path: '/', redirect: '/excel', hidden: true },
    { path: '*', redirect: '/404', hidden: true }
  

  // {
  //   path: '/',
  //   component: Layout,
  //   redirect: '/dashboard',
  //   name: '首页',
  //   hidden: true,
  //   children: [{ path: 'dashboard', component: dashboard, name: '首页'  }]
  // },

  
]
console.log(process.env.BASE_URL);
export default new Router({
  mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  // base: '/admin/',
  base: process.env.BASE_URL,
  routes: constantRouterMap
});

// export const asyncRouterMap = [
  
// ];
